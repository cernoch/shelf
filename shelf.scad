sirka = 30;
prumer = 38;
zaklad = 15;
mezi_nimi = 154;
mezi_vruty = 200;

$fn = 64;

    module nosic(spodek = true, vrsek = true)
    {
        translate([prumer+2*zaklad, 0]/2)
        {
            difference()
            {
                union()
                {
                    circle(d=prumer+2*zaklad);
                    if (spodek) {
                        translate(-[prumer/2+zaklad, prumer/2+zaklad])
                            square([prumer/2+zaklad, prumer/2+zaklad]);
                    }
                }
                circle(d=prumer);
                translate(-[prumer+zaklad, 0])
                    square(2*[prumer+zaklad, prumer+zaklad]);
            }

            // zakulaceni na spici
            translate([prumer+zaklad,0]/2)
                circle(d=zaklad);

            if (spodek)
            {
                difference()
                {
                    translate(-[prumer/2, prumer+zaklad])
                        square([prumer/2, prumer/2]);
                    translate(-[0, prumer+zaklad])
                        circle(d=prumer);
                }
            } 
           
            if (vrsek) {
                translate(-[prumer+zaklad,0]/2)
                    circle(d=zaklad);                
            }
        }
    }
    
module plocha()
{
    nosic(true, false);

    translate([0, mezi_nimi])
        nosic(true, true);

    // hlavni nosnik
    translate([0, -zaklad-prumer])
    {
        square([zaklad, mezi_nimi+prumer]);
    }

        translate([zaklad/2,-prumer-zaklad])
            circle(d=zaklad);                
    
}

dira = 8;

module sroub()
{
    translate([1-dira/2,0,zaklad/2])
        rotate([0, 90, 0])
        union(){
            cylinder(d=dira, h=zaklad+2);
            translate([0, 0, zaklad])
                cylinder(d1=dira, d2=2*dira, h=dira/2);

            translate([0, 0, zaklad+dira/2])
                cylinder(d=2*dira, h=dira);
        }

}

    difference()
    {
        linear_extrude(zaklad)
            plocha();

    
        translate([0, -50, 0])
        {
            sroub();
            translate([0, mezi_vruty, 0])
                sroub();
        }
    }    
