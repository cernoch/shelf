$fn=60;

// Diameter of the paddle
diameter = 29.5;
// Smoothing radius
smoothness = 2;
// Radius, adjusted for smoothness
radius = diameter/2 + smoothness;
// Distance between the centre and one paddle
// Set to 0 for only 1 paddle holder
distance = 35;
// Angle at which the paddles are inserted/removed
// This allows straps to firmly hold the paddles in place
angle = 45;

// Zip strap width
strap = 4;
// Bottom plate height
plate = 16;
// Thickness of the entire holder
thickness = 35;

// Thickness of the car's crossbar
crossbar_thickness = 33;
// Fin that aligns the holder on the crossbar
crossbar_overlap = 5;
// Smoothness of the crossbar edges
crossbar_smoothness = 3;



module strap_hole()
{
    module anti_strap_break()
    {
        scale([1, 0.25, 1])
            translate([0, 0, 10])
                rotate([45, 180, 0])
                    translate([-strap/2, -strap/2, -strap/2])
                        cube([strap, 10*strap, 10*strap]);
    }

    translate([-strap/2, -strap/2, 0])
        cube([strap, strap, thickness]);

    anti_strap_break();

    translate([0, 0, thickness])
        mirror([0, 0, 1])
            anti_strap_break();
}

module cross_section()
{
    minkowski()
    {
        circle(smoothness);

        difference()
        {
            // main block
            translate([ -distance -cos(angle)*radius,
                        -sin(angle)*radius ])
                square([ 2*distance +cos(angle)*2*radius,
                        radius +sin(angle)*radius + plate -smoothness*2]);

            // paddles
            translate([ -distance, 0 ]) circle(radius);
            translate([ +distance, 0 ]) circle(radius);

            // avoid small points on the corner
            translate([ -distance -cos(angle)*radius,
                        -sin(angle)*radius ]) circle(1);
            translate([ +distance +cos(angle)*radius,
                        -sin(angle)*radius ]) circle(1);
        }
    }
}


module crossbar()
{
    translate([ -distance -radius,
                plate +diameter/2 -crossbar_overlap +crossbar_smoothness,
                (thickness-crossbar_thickness)/2 +crossbar_smoothness])
        minkowski()
        {
            sphere(crossbar_smoothness);
            cube([ 2*distance +2*radius, 
                plate,
                crossbar_thickness]
                -[ 0,
                crossbar_smoothness,
                crossbar_smoothness ]*2);
        }
}

difference()
{
    // main block
    linear_extrude(thickness)
        cross_section();

    // holes for straps
    translate([-distance, radius + plate/2-smoothness, 0]) strap_hole();
    translate([+distance, radius + plate/2-smoothness, 0]) strap_hole();

    // hole for the crossbar
    crossbar();

    // write the diameter
    translate([0, diameter/2 + plate/2, thickness - 1])
        linear_extrude(3)
            rotate([0,0,180])
                text( str("⌀",diameter),
                      font="Cambria", size=6,
                      halign="center", valign="center");
}